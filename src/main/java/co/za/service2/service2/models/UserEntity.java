package co.za.service2.service2.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table
@Setter
@Getter
public class UserEntity {

    @Id
    @Column
    private long id;

    @Column
    private String name;

    @Column
    private LocalDate createdDate;

    @Column
    private LocalDate updatedDate;
}
