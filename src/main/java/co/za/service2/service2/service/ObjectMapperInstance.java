package co.za.service2.service2.service;

import com.fasterxml.jackson.databind.ObjectMapper;


public class ObjectMapperInstance {

    private static ObjectMapper objectMapper;
    private ObjectMapperInstance(){}

    public static ObjectMapper getInstance(){

        if (objectMapper == null){
            System.out.println("Create an instance");
            return new ObjectMapper();
        }
        return objectMapper;
    }

}
