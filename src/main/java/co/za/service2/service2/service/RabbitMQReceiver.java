package co.za.service2.service2.service;


import co.za.service2.service2.dto.UserDto;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.jarcasting.commons.json.JsonUtils;

import lombok.extern.slf4j.Slf4j;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
@RabbitListener(queues = "rabbitmq.queue", id = "listener")
@Slf4j
public class RabbitMQReceiver {

    @Autowired
    private UserServiceImpl userService;

    @RabbitHandler
    public void receiver(String userDto) {

        log.info("Received Message{}", userDto);

        if (userDto.contains("=")){

            userService.createOrUpdateUser(userDto);
        }else {

            userService.deleteUser(userDto);
        }



    }


}
