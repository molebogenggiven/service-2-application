package co.za.service2.service2.service;

import co.za.service2.service2.dto.UserDto;

public interface UserService {

    void createOrUpdateUser(String userDto);
    void deleteUser(String userId);

}
