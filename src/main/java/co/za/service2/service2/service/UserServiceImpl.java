package co.za.service2.service2.service;

import co.za.service2.service2.dao.UserRepository;
import co.za.service2.service2.dto.UserDto;
import co.za.service2.service2.models.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public void createOrUpdateUser(String userDto) {

        long id = Long.parseLong(userDto.split("=")[1].split(",")[0]);
        String name = userDto.split("=")[2].replaceAll("}","").replaceAll("'","");

        UserEntity userEntity = new UserEntity();
        userEntity.setName(name);
        userEntity.setUpdatedDate(LocalDate.now());
        if (!userRepository.findById(id).isPresent()){
            userEntity.setId(id);
            userEntity.setCreatedDate(LocalDate.now());
        }

        userRepository.save(userEntity);

    }

    @Override
    public void deleteUser(String userId) {

            if (userRepository.findById(Long.parseLong(userId)).isPresent()){

                userRepository.deleteById(Long.parseLong(userId));
            }

    }
}
